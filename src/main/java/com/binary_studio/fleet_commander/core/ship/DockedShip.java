package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSystem;

	private DefenciveSubsystem defenceSystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger pg,
			PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		this.attackSystem = subsystem;

		int pgVal = this.pg.value();
		int requiredPg = calculatePgConsumption();

		if (requiredPg > pgVal) {
			throw new InsufficientPowergridException(requiredPg - pgVal);
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		this.defenceSystem = subsystem;

		int shipPgAmount = this.pg.value();
		int requiredPg = calculatePgConsumption();

		if (requiredPg > shipPgAmount) {
			throw new InsufficientPowergridException(requiredPg - shipPgAmount);
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSystem == null && this.defenceSystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.defenceSystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		if (this.attackSystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}

		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitor, this.capacitorRegeneration,
				this.pg, this.speed, this.size, this.attackSystem, this.defenceSystem);
	}

	public int calculatePgConsumption() {
		int defenceConsumption = this.defenceSystem == null ? 0 : this.defenceSystem.getPowerGridConsumption().value();
		int attackConsumption = this.attackSystem == null ? 0 : this.attackSystem.getPowerGridConsumption().value();

		return (defenceConsumption + attackConsumption);
	}

}
