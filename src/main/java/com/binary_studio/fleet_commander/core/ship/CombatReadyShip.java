package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger startShieldHp;

	private PositiveInteger currentShieldHp;

	private PositiveInteger startHullHp;

	private PositiveInteger currentHullHp;

	private PositiveInteger startCapacitor;

	private PositiveInteger currentCapacitorAmount;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSystem;

	private DefenciveSubsystem defenceSystem;

	public CombatReadyShip(String name, PositiveInteger shieldHp, PositiveInteger hullHp, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size,
			AttackSubsystem attackSystem, DefenciveSubsystem defenceSystem) {
		this.name = name;
		this.startShieldHp = shieldHp;
		this.currentShieldHp = PositiveInteger.of(shieldHp.value());
		this.startHullHp = hullHp;
		this.currentHullHp = PositiveInteger.of(hullHp.value());
		this.startCapacitor = capacitor;
		this.currentCapacitorAmount = PositiveInteger.of(capacitor.value());
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
		this.attackSystem = attackSystem;
		this.defenceSystem = defenceSystem;
	}

	@Override
	public void endTurn() {
		int regenCapacitorVal = this.currentCapacitorAmount.value() + this.capacitorRegeneration.value();
		this.currentCapacitorAmount = PositiveInteger.of(Math.min(regenCapacitorVal, this.startCapacitor.value()));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.currentCapacitorAmount.value() < this.attackSystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() - this.attackSystem.getCapacitorConsumption().value());

		return Optional.of(new AttackAction(this.attackSystem.attack(target), this, target, this.attackSystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		int damageVal = this.defenceSystem.reduceDamage(attack).damage.value();
		int currHullHpVal = this.currentHullHp.value();
		int currShieldHpVal = this.currentShieldHp.value();
		int totalShipHp = currHullHpVal + currShieldHpVal;

		if (damageVal > totalShipHp) {
			return new AttackResult.Destroyed();
		}
		else if (damageVal > currShieldHpVal) {
			int damageRest = damageVal - currShieldHpVal;
			this.currentShieldHp = PositiveInteger.of(0);
			this.currentHullHp = PositiveInteger.of(currHullHpVal - damageRest);
		}
		else {
			this.currentShieldHp = PositiveInteger.of(currShieldHpVal - damageVal);
		}

		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damageVal), attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int currCapacitorVal = this.currentCapacitorAmount.value();
		int capacitorConsumptionVal = this.defenceSystem.getCapacitorConsumption().value();

		if (currCapacitorVal < capacitorConsumptionVal) {
			return Optional.empty();
		}

		this.currentCapacitorAmount = PositiveInteger.of(currCapacitorVal - capacitorConsumptionVal);

		final var regenerated = this.defenceSystem.regenerate();
		int hullHpVal = this.startHullHp.value();
		int regenHullHp = Math.min(hullHpVal - this.currentHullHp.value(), regenerated.hullHPRegenerated.value());
		int regenShieldHp = Math.min(hullHpVal - this.currentShieldHp.value(), regenerated.shieldHPRegenerated.value());

		return Optional.of(new RegenerateAction(PositiveInteger.of(regenShieldHp), PositiveInteger.of(regenHullHp)));
	}

}
