package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private static final int MAX_DAMAGE_PERCENT = 95;

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;
		this.capacitorConsumption = capacitorConsumption;
		this.powergridConsumption = powergridConsumption;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int incomingDamageVal = incomingDamage.damage.value();
		int damagePercent = Math.min(this.impactReductionPercent.value(), MAX_DAMAGE_PERCENT);
		int reducedDamage = (int) Math.floor(damagePercent * incomingDamageVal / 100.0);
		int finalDamage = Math.max(1, (incomingDamageVal - reducedDamage));

		return new AttackAction(PositiveInteger.of(finalDamage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
