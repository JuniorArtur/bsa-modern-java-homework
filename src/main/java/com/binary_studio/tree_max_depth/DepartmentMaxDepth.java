package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		int maxDepth = 0;

		final var departmentsQueue = new LinkedList<Department>();
		departmentsQueue.add(rootDepartment);

		while (true) {
			int departmentsCount = departmentsQueue.size();

			if (departmentsCount == 0) {
				return maxDepth;
			}
			++maxDepth;

			while (departmentsCount > 0) {
				final var currentLevelNode = departmentsQueue.remove();
				final var validChildrenNodes = currentLevelNode.subDepartments.parallelStream().filter(Objects::nonNull)
						.collect(Collectors.toList());

				departmentsQueue.addAll(validChildrenNodes);
				--departmentsCount;
			}
		}
	}

}
