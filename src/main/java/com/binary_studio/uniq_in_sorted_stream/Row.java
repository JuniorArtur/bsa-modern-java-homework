package com.binary_studio.uniq_in_sorted_stream;

import java.util.Objects;

public final class Row<T> {

	private final Long id;

	public Row(Long id) {
		this.id = id;
	}

	public Long getPrimaryId() {
		return this.id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Row)) {
			return false;
		}

		Row<? extends T> row = (Row<T>) o;

		return Objects.equals(this.id, row.id);
	}

	@Override
	public int hashCode() {
		return this.id != null ? this.id.hashCode() : 0;
	}

}
