package com.binary_studio.uniq_in_sorted_stream;

import java.util.HashSet;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		final var uniqueRows = new HashSet<Row<T>>();

		return stream.filter(item -> {
			if (uniqueRows.contains(item)) {
				return false;
			}
			uniqueRows.add(item);
			return true;
		});
	}

}
